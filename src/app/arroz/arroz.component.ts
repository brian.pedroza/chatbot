import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'arroz',
  templateUrl: './arroz.component.html',
  styleUrls: ['./arroz.component.css']
})
export class ArrozComponent implements OnInit {

  urlSinProcesar = "https://youtu.be/mXxLX-qDW1w";
  urlSaneada:any;
  constructor( private sanitizer: DomSanitizer ) { }

  ngOnInit(){
    this.urlSaneada = this.sanitizer.bypassSecurityTrustResourceUrl(this.urlSinProcesar);

  }
  


}
