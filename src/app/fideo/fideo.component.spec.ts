import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FideoComponent } from './fideo.component';

describe('FideoComponent', () => {
  let component: FideoComponent;
  let fixture: ComponentFixture<FideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
