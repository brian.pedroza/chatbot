import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EspaguetiComponent } from './espagueti.component';

describe('EspaguetiComponent', () => {
  let component: EspaguetiComponent;
  let fixture: ComponentFixture<EspaguetiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EspaguetiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EspaguetiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
