import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArrozComponent } from './arroz/arroz.component';
import { FideoComponent } from './fideo/fideo.component';



@NgModule({
  declarations: [ArrozComponent, FideoComponent],
  imports: [
    CommonModule
  ]
})
export class FoodModule { }
