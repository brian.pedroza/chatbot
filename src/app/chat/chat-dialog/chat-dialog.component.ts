import { Component, OnInit } from '@angular/core';
import { ChatService, Message } from 'src/app/services/chat.service';
import { Observable, of } from 'rxjs'; 
//import 'rxjs/add/operator/scan';
import { scan } from 'rxjs/operators';  
declare var $ : any;

@Component({
  selector: 'chat-dialog',
  templateUrl: './chat-dialog.component.html',
  styleUrls: ['./chat-dialog.component.css']
})
export class ChatDialogComponent implements OnInit {

  blnHideChat=true;
  messages: Observable<Message[]>;
  formValue: string;

  constructor( private chat : ChatService) { }

  ngOnInit(){
    this.messages = this.chat.conversation.asObservable()
    .pipe(
      scan((acc,val)=> acc.concat(val))
    );
    console.log("this.messages",this.messages);
  }

  sendMessagge(){
    this.chat.converse(this.formValue);
    this.formValue='';
  }

  fnShowChat(){
    this.blnHideChat=!this.blnHideChat;
    console.log("this.blnHideChat",this.blnHideChat);
  }

}
