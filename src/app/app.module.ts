import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChatModule } from './chat/chat.module';
import { PrincipalComponent } from './principal/principal.component';
import { ArrozComponent } from './arroz/arroz.component';
import { FideoComponent } from './fideo/fideo.component';
import { EspaguetiComponent } from './espagueti/espagueti.component';
@NgModule({
  declarations: [
    AppComponent,
    PrincipalComponent,
    ArrozComponent,
    FideoComponent,
    EspaguetiComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ChatModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
