import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrincipalComponent } from './principal/principal.component';
import { ArrozComponent } from './arroz/arroz.component';
import { FideoComponent } from './fideo/fideo.component';
import { EspaguetiComponent } from './espagueti/espagueti.component';


const routes: Routes = [
  {path:'',component: PrincipalComponent},
  {path:'arroz',component:ArrozComponent},
  {path:'fideo',component:FideoComponent},
  {path:'espagueti',component:EspaguetiComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
